//
//  SPViewController.m
//  FirstChallenge
//
//  Created by specktro on 04/02/14.
//  Copyright (c) 2014 specktro. All rights reserved.
//

#import "SPViewController.h"

@interface SPViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *validateButton;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) UILabel *textLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *yCenterConstraint;

@end

@implementation SPViewController

#pragma mark - IBAction Selectors
- (IBAction)validateStringAction:(UIButton *)sender {
    [self validateField];
}

#pragma mark - Private Selectors
- (void)validateField {
    if (![self.textField.text isEqual:@""]) {
        [self.textField resignFirstResponder];
        self.yCenterConstraint.constant = 200;
        
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.textField.alpha = self.validateButton.alpha = 0.0f;
                             [self.view layoutIfNeeded];
                         } completion:^(BOOL finished) {
                             self.textLabel.text = self.textField.text;
                             
                             [UIView animateWithDuration:0.3
                                                   delay:0.2
                                                 options:UIViewAnimationOptionBeginFromCurrentState
                                              animations:^{
                                                  self.textLabel.alpha = 1.0f;
                                              } completion:nil];
                         }];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"First Challenge"
                                                        message:@"Revisa que tu UITextField tenga información"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - UIViewController Life Cycle Selectors
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Se sube un poco el text field para que sea ocultado por el teclado en 3.5"
    self.yCenterConstraint.constant += 40;
    
    // Se crea el UILabel en donde vamos a colocar nuestro resultado
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    textLabel.translatesAutoresizingMaskIntoConstraints = NO;
    textLabel.alpha = 0.0f;
    [self.view addSubview:textLabel];
    self.textLabel = textLabel;
}

- (void)updateViewConstraints {
    [super updateViewConstraints];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.textLabel
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.textLabel
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0f
                                                           constant:0.0f]];
}

#pragma mark - UITextFieldDelegate Selectors
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self validateField];
    
    return YES;
}

@end